//
//  CircleButton.swift
//  FerdinandFolkFest
//
//  Created by Brandon Begle on 4/29/17.
//  Copyright © 2017 BW Mobile, LLC. All rights reserved.
//

import Foundation
import UIKit

class circleButton: UIButton {
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        layer.cornerRadius = layer.frame.height/2
        layer.frame.size = CGSize(width: layer.frame.height, height: layer.frame.height)
        layer.shadowOpacity = 0.7
        layer.shadowRadius = 4
        layer.shadowColor = UIColor.lightGray.cgColor
        layer.shadowOffset = CGSize.zero
        
    }
}
