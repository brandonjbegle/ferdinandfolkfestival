//
//  UINavigationBarBottomShadow.swift
//  FerdinandFolkFest
//
//  Created by Brandon Begle on 4/29/17.
//  Copyright © 2017 BW Mobile, LLC. All rights reserved.
//

import Foundation
import UIKit

class UINavigationBarBottomShadow: UINavigationBar {
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        layer.shadowOffset = CGSize.zero
        layer.shadowRadius = 4
        layer.shadowOpacity = 0.7
        layer.shadowColor = UIColor.lightGray.cgColor
        
        
    }
    
    
}
