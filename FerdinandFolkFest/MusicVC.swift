//
//  MusicVC.swift
//  FerdinandFolkFest
//
//  Created by Brandon Begle on 4/30/17.
//  Copyright © 2017 BW Mobile, LLC. All rights reserved.
//

import UIKit

class MusicVC: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.dataSource = self
        tableView.delegate = self

    }
    
}


extension MusicVC: UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "musicianCell", for: indexPath) 
        
        return cell
    }
    
    
}
