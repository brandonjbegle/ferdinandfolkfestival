//
//  ViewController.swift
//  FerdinandFolkFest
//
//  Created by Brandon Begle on 4/29/17.
//  Copyright © 2017 BW Mobile, LLC. All rights reserved.
//

import UIKit

class MainContainerVC: UIViewController {
    
    
    @IBAction func tabBtnClicked(_ sender: UIButton) {
        
        previousIndex = selectedIndex
        
        let previousVC = viewControllers[previousIndex]
        
        previousVC.willMove(toParentViewController: nil)
        
        previousVC.view.removeFromSuperview()
        
        previousVC.removeFromParentViewController()
        
        selectedIndex = sender.tag
        
        buttons[previousIndex].isSelected = false
        
        sender.isSelected = true
        
        let vc = viewControllers[selectedIndex]
        
        addChildViewController(vc)
        
        vc.view.frame = contentView.bounds
        
        contentView.addSubview(vc.view)
        
        vc.didMove(toParentViewController: self)
        
    }
    
    
    
    var foodViewController: UIViewController!
    var musicViewController: UIViewController!
    var infoViewController: UIViewController!
    var vendorsViewController: UIViewController!
    var eventsViewController: UIViewController!
    
    var viewControllers: [UIViewController]!
    
    @IBOutlet var buttons: [UIButton]!
    
    
    
    var previousIndex: Int = 0
    
    var selectedIndex: Int = 0

    @IBOutlet weak var contentView: UIView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        
        foodViewController = storyboard.instantiateViewController(withIdentifier: "foodViewController")
        musicViewController = storyboard.instantiateViewController(withIdentifier: "musicViewController")
        infoViewController = storyboard.instantiateViewController(withIdentifier: "infoViewController")
        vendorsViewController = storyboard.instantiateViewController(withIdentifier: "vendorsViewController")
        eventsViewController = storyboard.instantiateViewController(withIdentifier: "eventsViewController")
        
        viewControllers = [musicViewController, foodViewController, vendorsViewController, eventsViewController, infoViewController]
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        let logo = UIImage(named: "logo.png")
        let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 159, height: 40))
        imageView.image = logo
        imageView.contentMode = .scaleAspectFit
        self.navigationItem.titleView = imageView
        
        buttons[selectedIndex].isSelected = true
        
        tabBtnClicked(buttons[selectedIndex])

    }

    
    
    
    

}

